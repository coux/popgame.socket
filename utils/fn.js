import filter from "fxjs/Lazy/filterL";
import map from "fxjs/Lazy/mapL";
import go from "fxjs/Strict/go";
import add from "fxjs/Strict/add";
import reduce from "fxjs/Strict/reduce";

export const arrayFrom = (count, adder = 1) => Array.from(Array(count).keys()).map(n => n + adder);
export const arrayShuffle = arr => arr.sort(() => Math.random() - 0.5);
/**
	 * 게임 리액션 처리
	 * @param {array} a
	 * @param {array} b
	 * @memberof Game
	 * a배열의 수와 a배열의 id 값이 포함된 b배열의 수가 같은지 판단
	 */
export const compare = (a, b) =>
	a.length == go(
		b,
		filter(user => a.find(u => u.id === user.id)),
		map(() => 1),
		reduce(add)
	)

/**
 * 게임 점수 기록
 * @param {string, string, number} { id, nickname, score }
 * @param {array} arr
 * @memberof Game
 */
export const scoreSorting = (arr, data) => {
	const index = arr.findIndex(el => el.id === data.id);
	if (index > -1) {
		arr[index] = data;
	} else {
		arr = [...arr, data];
	};
	return arr.sort((a, b) => b.score - a.score)
}


export const numeric = (str) => parseInt(str.replace(/\D/g, ''));
