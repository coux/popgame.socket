module.exports = {
	/**
	 * 앱 설정
	 */
	apps: [
		{
			// 이름. 나중에 이 이름으로 프로세스를 관리한다
			name: "popgame-socket",
			script: "./index.js", // 실행할 파일 경로
			instances: 1, // 클러스터 모드 사용 시 생성할 인스턴스 수
			exec_mode: "fork", // fork, cluster 모드 중 선택
			interpreter: "./node_modules/.bin/babel-node",
			merge_logs: true, // 클러스터 모드 사용 시 각 클러스터에서 생성되는 로그를 한 파일로 합쳐준다.
			autorestart: true, // 프로세스 실패 시 자동으로 재시작할지 선택
			watch: true, // 파일이 변경되었을 때 재시작 할지 선택
			max_memory_restart: "512M", // 프로그램의 메모리 크기가 일정 크기 이상이 되면 재시작한다.
			log_date_format: "YYYY-MM-DD HH:mm Z",
			env: {
				name: "popgame-socket.develop",
				PORT: 8888,
				COMMON_VARIABLE: 'true',
				NODE_ENV: "development"
			},
			env_production: {
				name: "popgame-socket.master",
				PORT: 8000,
				COMMON_VARIABLE: 'true',
				NODE_ENV: "production"
			}
		}
	],
	/**
	 * 배포 설정
	 */
	deploy: {
		development: {
			// 개발 환경설정
			key: '~/keys/popsocket',
			user: "root",
			host: [{ host: '114.31.42.27', port: '2222' }],  // 서버 도메인 또는 IP, 포트"
			ref: "origin/develop", // 리모트 브랜치
			repo: 'ssh://gitlab@gitlab.theenm.com:7777/Lim_jinwoo/popgame-socket.git', // Github 프로젝트 주소
			ssh_options: ['StrictHostKeyChecking=no', 'PasswordAuthentication=no', 'ForwardAgent=yes'], // SSH 접속 옵션.
			path: '/var/www/popgame-socket/develop', // 원격 서버에서 프로젝트를 생성할 위치
			// 프로젝트 배포 후 실행할 명령어
			'post-deploy': 'npm install --only=development && pm2 startOrRestart ecosystem.config.js --env development'
		},
		production: {
			key: '~/keys/popsocket',
			user: 'root', // 접속할 계정. SSH를 사용해서 서버에 접속할 수 있어야 한다.
			host: [{ host: '114.31.42.27', port: '2222' }], // 서버 도메인 또는 IP
			ref: 'origin/master', // 서버에서 clone할 브랜치
			repo: 'ssh://gitlab@gitlab.theenm.com:7777/Lim_jinwoo/popgame-socket.git', // Git 저장소 URL
			ssh_options: ['StrictHostKeyChecking=no', 'PasswordAuthentication=no', 'ForwardAgent=yes'], // SSH 접속 옵션.
			path: '/var/www/popgame-socket/master', // 앱을 설치할 폴더 위치
			// PM2가 배포(git clone)한 후 실행할 명령어
			'post-deploy': 'npm install --only=production && pm2 startOrRestart ecosystem.config.js --env production'
		},
	},
}