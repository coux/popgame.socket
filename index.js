import http from 'http';
import express from 'express';
import socketIO from 'socket.io';
import User from './stores/User';
import Global from './stores/Global';
const port = process.env.PORT || 8000;
const app = express();

const server = http.createServer(app);
server.listen(port, () => console.log(`Listening on port ${port}`));

/** 소켓서버 */
const io = socketIO(server);

/**
 * @param {object} 'socket'
 * 
 * 클라이언트에 2초 간격으로 신호를 보냄
 * io.set('heartbeat interval', 25 * 1000);
 * 만약 1초동안 응답이 없다면 접속 종료로 간주함.
 * io.set('heartbeat timeout', 60 * 1000);
 */
// io.set('heartbeat interval', 1000);
// io.set('heartbeat timeout', 2000);

io.on('connection', socket => {
	const { id } = socket;
	if (!Global.io) {
		Global.io = io;
	}

	const user = new User({ id });
	socket.emit('connected');
	socket.on('disconnect', user.disconnect);
	socket.on('login', user.login);
	socket.on('logout', user.logout);
	socket.on('createRoom', user.createRoom);
	socket.on('joinRoom', user.joinRoom);
	socket.on('leaveRoom', user.leaveRoom);
	socket.on('navigateTo', user.navigateTo);
});