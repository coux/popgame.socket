
import Global from './Global';
import Lobby from './Lobby';
import Room from './Room';

export default class User {

	/**
	 * 로비 입장
	 * @param {object} { nickname: String }
	 * @returns {void}
	 * @memberof User
	 */
	login = ({ nickname }) => {
		this.nickname = nickname;
		const socket = Global.socket(this.id);
		socket.join('lobby', () => {
			// 클라이언트에게 로비 입을 알림
			socket.emit('logined', this);
			// 로비의 방목록을 가져옴
			socket.emit('rooms', Lobby.rooms);
		});
	}

	/**
	 * 로비 퇴장
	 * @returns {void}
	 * @memberof User
	 */
	logout = () => {
		const socket = Global.socket(this.id);
		// 클라이언트에게 로비 퇴장을 알림
		socket.leave('lobby', () => {
			socket.emit('logouted');
		});
	}

	/**
	* 서버 접속 종료
	* @returns {void}
	* @memberof User
	*/
	disconnect = () => {
		Global.removeUser(this.id);
		this.leaveRoom();
	}

	/**
	* 방 생성
	* @returns {void}
	* @memberof User
	*/
	createRoom = () => {
		const { id } = this;
		const socket = Global.socket(id);
		if (socket) {
			// 같은 아이디로 이미 만들어진 방이 있는지 검사
			const room = Lobby.findRoom({ id });
			// const room = Global.room(id);
			// 있다면
			room ?
				// 방을 만들수 없다고 메세지 전송
				socket.emit('err', '방을 만들 수 없습니다. 방이 이미 있습니다.')
				// 없다면
				: socket.join(id, () => {
					// 방을 새로 만들고 방장을 자신으로 임명
					const room = new Room({ owner: this });
					// 나갈때를 대비해 로비에서 방을 검색해야 되므로 방의 아이디를 저장
					// this.room = room 설정시 심각한 mobx 오류에 직면할 것임.
					this.roomId = room.id;
				})
		} else {
			console.log('오류가 발생했습니다. 방을 만들지 못했습니다.');
		}
	}

	/**
	 * 로비 입장
	 * @param {object} { id: Strig }
	 * @returns {void}
	 * @memberof User
	 */
	joinRoom = ({ id }) => {
		const socket = Global.socket(this.id);
		if (socket) {
			// 입장할 방이 있는지 로비에서 검사
			let room = Lobby.findRoom(id);
			// 있다면
			room ?
				// 소켓을 먼저 조인 시키고
				socket.join(this.id, () => {
					// 방에 입장
					room.join({ user: this });
					// 나갈때를 대비해 로비에서 방을 검색해야 되므로 방의 아이디를 저장
					this.roomId = room.id;
				}) :
				// 없다면 방에 입장할수 없다고 메세지 전송
				socket.emit('err', '방에 입장 할수 없습니다. 방이 없습니다.');
		} else {
			console.log('err', '서버에서 오류가 발생했습니다.');
		}
	};

	/**
	* 방 퇴장
	* @returns {void}
	* @memberof User
	*/
	leaveRoom = () => {
		// 로비에서 퇴장할 방이 존재 하는지 검사
		const room = Lobby.findRoom(this.roomId);
		// 있다면 방에서 나옴.
		if (room) {
			// if (room.game) {
			// 	console.log('user game leave ====>');
			// 	room.game.leave({ user: this });
			// }
			room.leave({ user: this });
		}
	}


	/**
	 * 네이티브 & 소켓 라우팅
	 * @memberof Store
	 * @param  {string} routeName
	 * @param  {object} params
	 */
	navigateTo = ({ routeName, params }) => {
		const room = Lobby.findRoom(this.roomId);
		if (room) {
			Global.roomClients(room.id).forEach(socket => {
				socket.emit('navigateTo', { routeName, params });
			})
		}
	};


	removeListener = ({ id }) => {
		const socket = Global.socket(id);
		console.log('socket: ', socket);
		// socket.removeAllListeners();
	}

	/**
	 *Creates an instance of User.
	 * @param {*} { id:String }
	 * @memberof User
	 */
	constructor({ id }) {
		this.id = id;
		this.nickname = null;
		this.roomId = null;
		this.groupId = null;
		// 글로벌 유저에 유저 정볼르 저장
		Global.addUser(this);
	}
}