import { observable, reaction } from 'mobx';
import Game from './Game';
import Global from './Global';
import Lobby from './Lobby';
import Group from './Group';


class Room {

	/**
	 * 감시해야 될 방유저 목록
	 * @memberof Room
	 */
	@observable users = [];

	/**
	 * 감시해야 될 선택된 게임
	 * @memberof Room
	 */
	@observable game = null;

	// /**
	//  * 감시해야 될 유저들의 게임버전
	//  * @memberof Room
	//  */
	// @observable games = {};

	/**
	 * 감시해야 될 방장 정보
	 * @memberof Room
	 */
	@observable owner = null;


	/**
	 * 감시해야 될 팀목록
	 * @memberof Room
	 */
	@observable groups = [];


	/**
	 * 감시해야 될 유저들의 게임버전
	 * @memberof Room
	 */
	@observable games = {};

	/**
	 * 아이디로 방 유저 찾기
	 * @param {string} { id }
	 * @returns {object}
	 * @memberof Room
	 */
	find = ({ id }) => {
		return this.users.find(u => u.id === id);
	}

	/**
	 * 방 입장
	 * @param {object} { user }
	 * @returns {void}
	 * @memberof Room
	 */
	join = ({ user }) => {
		const exist = this.find({ id: user.id });
		if (!exist) {
			// 클라이언트에 방정보를 전달
			const socket = Global.socket(user.id);
			if (socket) {
				this.resetHandler(socket);
				socket.emit('room', this);
				socket.on('ban', this.ban);
				socket.on('owner', this.setOwner);
				socket.on('selectGame', this.selectGame);
				socket.on('leaveGame', this.leaveGame({ user }));
				socket.on('userGames', this.userGames({ user }));
			}
			// 유저 목록 갱신
			this.users = [...this.users, user];
		}
	}


	/**
	 * 방 퇴장
	 * @param {object} { user }
	 * @returns {void}
	 * @memberof Room
	 */
	leave = ({ user }) => {
		this.delegate({ user });
		if (this.game) {
			console.log('room game leave2 ====>');
			this.game.leave({ user });
		}
		const socket = Global.socket(user.id);
		if (socket) {
			this.resetHandler(socket);
		}
		// if (this.group) {
		// 	this.leaveGroup({ user });
		// }
		delete this.games[user.id];
		this.users = this.users.filter(u => u.id !== user.id);
		if (this.users.length === 0) {
			Lobby.removeRoom(this);
			Global.io.to('lobby').emit('rooms', Lobby.rooms);
			this.reactions.forEach(reaction => reaction.dispose())
		}
	}

	/**
	 * 게임 핸들러 초기화
	 */
	resetHandler = socket => {
		if (socket._events) {
			const { selectGame, leaveGame, ban, owner, userGames, ...rest } = socket._events;
			socket._events = rest;
		}
	}

	/**
	* 퇴출
	* @memberof Room
	* @param {string} {id}
	*/
	ban = ({ id }) => {
		const user = this.find({ id });
		if (user) {
			this.leave({ user });
			Global.socket(id).emit('disconnected', '방장에게 강퇴 당했습니다.');
		}
	};


	/**
	 * 방장 인계자 찾기
	 * @memberof Room
	 */
	delegate = ({ user }) => {
		if (user.id === this.owner.id) {
			const index = this.users.findIndex(u => u.id === user.id);
			const owner = this.users.length > 1 && this.users[index + 1];
			if (owner) {
				this.setOwner({ id: owner.id });
			}
		}

	}


	/**
	 * 방장 권한 인계
	 * @param {object} { user }
	 * @memberof Room
	 */
	setOwner = ({ id }) => {
		const user = this.find({ id });
		if (user) {
			const users = this.users.filter(u => u.id !== user.id)
			this.users = [user, ...users];
			this.owner = user;
		}
	}

	/**
	 * 개별 게임 버전 관리
	 * @memberof Room
	 */
	userGames = ({ user }) => ({ games }) => {
		this.games[user.id] = games;
	}

	/**
	 * 게임 버전 유효성 검사
	 * @param {object} { data }
	 * @memberof Room
	 */
	verifyGame = game => {
		return Object.keys(this.games).reduce((acc, key) => {
			const value = this.games[key];
			const { name, enabled, updated } = value.find(g => g.id === game.id);
			if (!enabled || !updated) {
				acc = [...acc, { key, name, enabled, updated }];
			}
			return acc;
		}, []);
	}


	/**
	 * 게임 유효성 검사 결과 전송
	 * @param {object} { data }
	 * @memberof Room
	 */
	repairMessage = (game, results) => {
		const sockets = Global.roomClients(this.id);
		sockets.forEach(socket => {
			const invalid = results.find(result => result.key === socket.id)
			if (invalid) {
				const { enabled, updated } = invalid;
				game.autoDownload = enabled === false;
				game.autoUpdate = updated === false;
				let message = enabled === false ?
					`게임이 설치 되지 않았습니다. \n 다운로드 하시겠습니까?`
					: updated === false ?
						`게임이 업데이트 되었습니다. \n 업데이트 하시겠습니까?`
						: '';
				socket.emit('message', { type: 'confirm', message, game });
			} else {
				socket.emit('message', { type: 'alert', message: `다른 사용자가 게임을 설치 중 입니다. \n 잠시 기다려주세요.` });
			}
		})
	}


	/**
	 * 게임 생성
	 * @param {object} { data }
	 * @memberof Room
	 */
	selectGame = ({ game }) => {
		const { users } = this;
		if (game) {
			const results = this.verifyGame(game);
			if (results.length > 0) {
				return this.repairMessage(game, results);
			}
			this.game = new Game({ ...game, roomid: this.id });
			users.forEach(user => this.game.join({ user }));
		} else {
			if (this.game) {
				users.forEach(user => this.game.leave({ user }));
				this.game = null;
			}
		}
	}


	leaveGame = ({ user }) => () => {
		if (this.game) {
			this.game.leave({ user });
			Global.roomSockets(this.id).emit('leavedGame');
		}
	}

	// /**
	//  * 팀 자동 탈퇴
	//  * @param {function} { user }
	//  * @memberof Room
	//  */
	// leaveGroup = ({ user }) => {
	// 	const group = this.groups.find(group => group.members.find(u => u.id === user.id));
	// 	if (group) {
	// 		this.groups = this.groups.filter(group => group.leave({ user }).length > 0);
	// 	}
	// }
	// /**
	//  * 팀 만들기/조인
	//  * @param {function} { user } => { name } => void
	//  * @memberof Room
	//  */
	// selectGroup = ({ user }) => ({ name }) => {
	// 	this.leaveGroup({ user });
	// 	let group = this.groups.find(group => group.name === name);
	// 	if (!group) {
	// 		group = new Group({ name, room: this });
	// 		this.groups = [...this.groups, group];
	// 	}
	// 	const index = this.users.findIndex(u => u.id === user.id);
	// 	this.users[index] = { ...user, group };
	// 	group.join({ user });
	// }


	/**
	 *Creates an instance of Room.
	 * @param {object} { user }
	 * @memberof Room
	 */
	constructor({ owner }) {
		this.owner = owner;
		this.id = owner.id;
		this.name = `room-${this.id}`;
		this.reactions = [];

		// 방이 만들어지면 로비에 추가해줌
		Lobby.addRoom(this);
		// 유저들 변동이 감지되면 방 유저들 과 로비유저들에게 현제 방정보를 전달
		reaction(() => this.users, (users, reaction) => {
			// 방나갈때 리액션 제거해주기 위해
			this.reactions = [...this.reactions, reaction]
			// 멤버들의 정보를 갱신해줌
			Global.roomSockets(this.id).emit('room.users', users);
			// 로비쪽에도 갱신 해줌
			Global.io.to('lobby').emit('rooms', Lobby.rooms);
			console.log(`updated room users =>`, users.map(user => user.id));
		});


		/** 게임 변동이 감지되면 방 유저들에게 게임 정보를 전달 */
		reaction(() => this.game, (game, reaction) => {
			this.reactions = [...this.reactions, reaction];
			Global.roomSockets(this.id).emit('room.game', this.game);
			console.log(`updated room game =>`, game ? game.name : 'null');
		});

		// /** 호스트가 변경되면 방 유저들에게 게임정보를 전달 */
		reaction(() => this.owner, (owner, reaction) => {
			this.reactions = [...this.reactions, reaction];
			Global.roomSockets(this.id).emit('room.owner', this.owner);
			console.log(`updated room owner =>`, owner.id);
		});

		this.join({ user: owner });
	}
}

export default Room;