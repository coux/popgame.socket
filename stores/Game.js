import { observable, reaction } from 'mobx';
import Global from './Global';
import Turn from './rules/Turn';
import Basic from './rules/Basic';
import { compare, scoreSorting } from '../utils/fn';



const rules = {
	Turn,
	Basic
}


class Game {

	/**
	 * 감시해야 될 게임 유저 목록
	 * @memberof Game
	 */
	@observable users = [];
	@observable players = []
	@observable results = [];
	@observable records = [];
	@observable time = 0;


	/**
	 * 게임 입장
	 * @param {object} { user }
	 * @returns void
	 * @memberof Game
	 */
	join = ({ user }) => {
		const found = this.users.find(u => u.id === user.id);
		this.users = found ? this.users : [...this.users, user];
		const socket = Global.socket(user.id);
		if (socket) {
			this.resetHandler(socket);
			socket.on('readyGame', this.readyHandler({ user }));
			socket.on('gameEvent', this.eventHandler({ user }));
			socket.on('endGame', this.endHandler({ user }));
			socket.on('startGame', this.startHandler);
			socket.on('startTimer', this.timerHandler);
		}
	}


	/**
	 * 게임 퇴장
	 * @param {object} { user }
	 * @returns void
	 * @memberof Game
	 */
	leave = ({ user }) => {
		console.log('game leave');
		this.users = this.users.filter(u => u.id !== user.id);
		this.players = this.players.filter(p => p.id !== user.id);
		this.results = this.results.filter(r => r.id !== user.id);
		this.records = this.rule.leave({ user });

		const socket = Global.socket(user.id);
		if (socket) {
			this.resetHandler(socket);
		}
		Global.removeTimer(this.roomid);
		if (this.users.length === 0) {
			this.reactions.forEach(reaction => reaction.dispose())
		}
	}

	/**
	 * 게임 시작 핸들러
	 * @param {boolean} { restart }
	 * @memberof Game
	 */
	startHandler = (args) => this.start(args);

	/**
	 * 게임점수 등록 핸들러
	 * @param {number} { score }
	 * @memberof Game
	 */
	eventHandler = ({ user }) => (data) => this.event({ ...user, ...data });


	/**
	 * 게임종료 핸들러
	 * @param {number} { score }
	 * @memberof Game
	 */
	endHandler = ({ user }) => ({ score, penalty }) => this.end({ ...user, score, penalty });

	/**
	 *
	 * 게임시간 설정
	 * @param {number} { time }
	 * @memberof Game
	 */
	timerHandler = () => {
		const start = Date.now();
		const time = this.timeLimit;
		const timer = () => {
			const current = time > 0 ? time * 1000 - (Date.now() - start) : Date.now() - start;
			if (current < 0) {
				this.time = 0;
				Global.removeTimer(this.roomid);
			}
			this.time = current;
		}
		Global.addTimer(this.roomid, setInterval(timer, 1000))
	}

	/**
	 * 게임 준비
	 * @param {object} { user }
	 * @memberof Game
	 */
	readyHandler = ({ user }) => ({ timeLimit, rules }) => {
		const found = this.players.find(u => u.id === user.id);
		this.players = found ? this.players : [...this.players, user];
		this.timeLimit = timeLimit;
		this.rule.setRule({ rules });
		this.rule.join({ user });
	}

	/**
	 * 게임 핸들러 초기화
	 */
	resetHandler = socket => {
		if (socket._events) {
			const { readyGame, gameEvent, endGame, startGame, startTimer, ...rest } = socket._events;
			socket._events = rest;
		}
	}


	/**
	 * 게임 시작
	 * @param {object} { restart }
	 * @memberof Game
	 * 게임 시작 / 재시작
	 */
	start = ({ restart }) => {
		Global.removeTimer(this.roomid);
		restart && this.reset();
		this.time = 0;
		this.records = this.rule.start();
		this.status = 'playing';
		// console.log('this.records: ', JSON.parse(JSON.stringify(this.records)));
		this.penalties = [];
		Global.roomSockets(this.roomid).emit('startedGame', { restart });
	};


	/**
	 * 게임 종료
	 * @param {id} { string }
	 * @param {nickname} { string }
	 * @param {score} { number }
	 * @memberof Game
	 */
	end = ({ id, nickname, score, penalty }) => {
		this.penalties = [...this.penalties, penalty];
		Global.removeTimer(this.roomid);
		this.results = scoreSorting([...this.results], { score, id, nickname, penalty });
		this.status = 'pending';
	};


	/**
	 * 게임 점수 저장
	 * @param {id} { string }
	 * @param {nickname} { string }
	 * @param {score} { number }
	 * @memberof Game
	 * 중간 데이터 싱크용
	 */
	event = ({ id, nickname, score, select }) => {
		this.records = this.rule.applies([...this.records], { score, id, nickname, select });
		// console.log('this.records: ', JSON.parse(JSON.stringify(this.records)));
	};


	/**
	 * 게임 초기화
	 * @memberof Game
	 */
	reset = () => {
		this.records = [];
		this.results = [];
	}


	/**
	 *Creates an instance of Game.
	 * @param {string, string, string} { name, url, id }
	 * @memberof Game
	 */
	constructor({ name, url, id, type, roomid }) {
		this.id = id;
		this.roomid = roomid;
		this.url = url;
		this.name = name;
		this.penalties = [];
		this.reactions = [];
		this.status = 'pending';
		this.rule = new rules[type]();
		this.timeLimit = 0;

		/** 유저목록에 변화가 감지되면 클라이언트의 게임 데이터를 교체함  */
		reaction(() => this.users, (users, reaction) => {
			this.reactions = [...this.reactions, reaction]
			Global.roomSockets(this.roomid).emit('room.game.users', users);
		});

		/** 게임 참가자 목록에 변화가 감지되면 클라이언트의 게임 데이터를 교체함  */
		reaction(() => this.players, (players, reaction) => {
			this.reactions = [...this.reactions, reaction];
			Global.roomSockets(this.roomid).emit('room.game.players', players);
			console.log(`updated game players =>`, players.map(p => p.id));
			/** 겜 참여자 모두 준비 완료된 상태 */
			if (compare(this.users, players)) {
				if (this.status === 'pending') {
					Global.roomSockets(this.roomid).emit('readyGame', this.rule);
					console.log('all ready?', players.length, compare(this.users, players));
				}
			}
		});


		reaction(() => this.results, (results, reaction) => {
			this.reactions = [...this.reactions, reaction];
			/** 겜 참여자 모두 게임 종료 된 상태 */
			if (compare(this.users, results)) {
				const penalty = this.penalties.find(penaty => penaty !== 'undefined');
				Global.roomSockets(this.roomid).emit('endedGame', { results, penalty });
				console.log('All players finished the game', this.players.map(({ id }) => id));
			};
		});

		reaction(() => this.records, (records, reaction) => {
			console.log('records: ', JSON.parse(JSON.stringify(records)));
			this.reactions = [...this.reactions, reaction]
			/** 겜 데이터 중간 정보 보내기 */
			Global.roomSockets(this.roomid).emit('gameData', { records });
		});

		reaction(() => this.time, (time, reaction) => {
			this.reactions = [...this.reactions, reaction]
			/** 겜 남은 시간 보내기 */
			Global.roomSockets(this.roomid).emit('remainTime', { time: Date.now() });
		});
	}
}

export default Game;