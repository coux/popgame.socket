
import { observable, reaction, action } from 'mobx';
import Lobby from './Lobby'
const singleton = Symbol();
const singletonEnforcer = Symbol();

class Global {
	@observable users = {};
	@observable timers = {};

	/**
	 *Creates an instance of Global.
	 * @param {Symbol} enforcer
	 * @memberof Global
	 */
	constructor(enforcer) {
		if (enforcer !== singletonEnforcer) {
			throw new Error('Cannot construct singleton');
		}
		this.io = null;
		reaction(() => this.users, users => {
			console.log('Global users ->', Object.keys(users));
		})
	}

	/* 현재 접속되어 있는 접속 소켓 */
	@action
	socket = (id) => {
		return this.io.sockets.connected[id];
	}

	/* 방의 모든 멤버들에게 메세지를 전달 */
	@action
	roomSockets = (roomid) => {
		const emit = (event, data) => this.roomClients(roomid).forEach(socket => {
			if (socket) {
				socket.emit(event, data)
			}
		});
		return { emit }
	}

	/* 방의 모든 멤버들의 소켓리스트 */
	@action
	roomClients = (roomid) => {
		const room = Lobby.findRoom(roomid);
		return room ? room.users.map(user => this.socket(user.id)) : [];
	}


	@action
	findUser = (id) => {
		return this.users.find(user => user.id === id);
	}

	@action
	addUser = (user) => {
		this.users = { ...this.users, [user.id]: user }
	}

	@action
	removeUser = (id) => {
		const { [id]: value, ...rest } = this.users;
		this.users = rest;
	}

	addTimer = (id, timer) => {
		this.removeTimer(id)
		this.timers[id] = timer;
	}

	removeTimer = (id) => {
		if (this.timers[id]) {
			clearInterval(this.timers[id]);
			delete this.timers[id]
		}
	}

	timer = (id) => {
		return this.timers[id];
	}


	/**
	 * 싱글톤 객체
	 * @readonly
	 * @static
	 * @memberof Global
	 */
	static get instance() {
		if (!this[singleton]) {
			this[singleton] = new Global(singletonEnforcer);
		}
		return this[singleton];
	}
}

export default Global.instance;