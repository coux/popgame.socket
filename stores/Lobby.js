import { observable, reaction, toJS } from 'mobx';
import Global from './Global';
const singleton = Symbol();
const singletonEnforcer = Symbol();

class Lobby {
	/**
	 * 감시해야 될 방 목록
	 * @memberof Lobby
	 */
	@observable rooms = [];
	/**
	 *Creates an instance of Lobby.
	 * @param {Symbol} enforcer
	 * @memberof Lobby
	 */
	constructor(enforcer) {
		if (enforcer !== singletonEnforcer) {
			throw new Error('Cannot construct singleton');
		}

		/** 방목록에 변동감지 되면 로비 유저에게 정보전달 */
		reaction(() => this.rooms, () => {
			Global.io.to('lobby').emit('rooms', this.rooms);
			console.log('updated rooms =>', this.rooms.map(room => room.id));
		});
	}

	/**
	 * 아이디로 방 찾기
	 * @param {string} { id }
	 * @returns {object}
	 * @memberof Lobby
	 */
	findRoom = (id) => {
		return this.rooms.find(room => room.id == id);
	}


	/**
	 * 방 목록에 방추가
	 * @param {object} room
	 * @returns {array}
	 * @memberof Lobby
	 */

	addRoom = (room) => {
		const exist = this.findRoom(room.id);
		this.rooms = exist ? this.rooms : [...this.rooms, room];
	}


	/**
	 * 방 목록에서 방 제거
	 * @param {object} room
	 * @returns {array}
	 * @memberof Lobby
	 */

	removeRoom = (room) => {
		this.rooms = this.rooms.filter(r => r.id !== room.id);
	}



	/**
	 * 싱글톤 객체
	 * @readonly
	 * @static
	 * @memberof Lobby
	 */

	static get instance() {
		if (!this[singleton]) {
			this[singleton] = new Lobby(singletonEnforcer);
		}
		return this[singleton];
	}
}

export default Lobby.instance;
