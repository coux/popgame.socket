import { observable, reaction } from 'mobx';
import Global from './Global';

class Group {
	@observable members = [];

	/**
	 * 그룹 입장
	 * @param {object} { user }
	 * @returns {void}
	 * @memberof Group
	 */
	join = ({ user }) => {
		const found = this.members.find(u => u.id === user.id);
		this.members = found ? this.members : [...this.members, user];
		return this.members
	}

	/**
	 * 그룹 퇴장
	 * @param {object} { user }
	 * @returns {void}
	 * @memberof Group
	 */
	leave = ({ user }) => {
		this.members = this.members.filter(u => u.id !== user.id);
		return this.members;

	}

	constructor({ name, room }) {
		this.name = name;
		const io = Global.io[room.id];
		/** 그룹멤버가 변경되면 방 유저들에게 그룹정보를 전달 */
		reaction(() => this.members, members => {
			io.to(room.id).emit('room.groups', room);
			console.log(`updated group members =>`, members.map(user => user.id));
		});
	}
}


export default Group;