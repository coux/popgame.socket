
import Rule from './Rule';
import { scoreSorting } from '../../utils/fn'

class Basic extends Rule {

	/**
	 * 퀘스트 결과를 전송
	 * @param  {array} arr
	 * @param  {number} {select}
	 */
	applies(arr, player) {
		return scoreSorting(arr, player);
	}

	/**
	 * 게임 시작
	 * @returns array
	 * @memberof Basic
	 */
	start() {
		return this.players.map(({ id, nickname }) => ({ id, nickname, score: 0 }));
	}

	/**
	 * 게임 퇴장
	 * @param {object} { user }
	 * @returns array
	 * @memberof Basic
	 */
	leave({ user }) {
		super.leave({ user });
		return this.players.map(({ id, nickname }) => ({
			id,
			nickname,
			score: 0
		}));
	}
}

export default Basic;