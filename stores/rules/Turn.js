

import Rule from './Rule';
import { arrayFrom, arrayShuffle, numeric } from '../../utils/fn'

class Turn extends Rule {

	/**
	 * 기본 옵션설정
	 * @param {object} { rules }
	 * @memberof Turn
	 */
	setRule({ rules }) {
		super.setRule({ rules });
		const { random, type } = rules;
		// 퀘스트 포맷 생성 ex) 1 -> A, 2 -> B, 3 -> C, 4 -> D 형태로 변환
		this.format = arrayFrom(type)
			.reduce((acc, cur) => acc.concat(arrayFrom(random)
				.map(n => `${String.fromCharCode(cur + 64)}${n}`))
				, []);
		// 퀴스트 포멧을 랜덤으로 섞음
		this.quests = arrayShuffle([...this.format]);
	}

	/**
	 * 지정된 플레이어 순서 리스트에서 하나씩 빼옴
	 * @returns
	 * @memberof Turn
	 */
	getTurn() {
		if (this.pattern.length === 0) {
			return { prev: null, next: null }
		}
		if (this.orders.length === 0) {
			this.orders = [...this.pattern]
		}

		return {
			prev: this.turn && this.turn.next,
			next: this.orders.splice(0, 1)[0].id
		}
	}


	/**
	 * 지정된 퀘스트 리스트에서 하나씩 빼옴
	 * @returns
	 * @memberof Turn
	 */
	getQuest() {
		if (this.quests.length === 0) {
			this.quests = arrayShuffle([...this.format])
		}
		return this.quests.splice(0, 1)[0];
	}

	/**
	 * 퀘스트 결과를 전송
	 * @param  {array} arr
	 * @param  {number} {select}
	 */
	applies(arr, { select }) {
		let correct = false;
		const prev = this.quest;
		const next = this.getQuest();
		if (numeric(prev) > numeric(next)) {
			correct = select === -1;
		} else if (numeric(prev) < numeric(next)) {
			correct = select === 1;
		} else {
			correct = select === 0;
		}

		if (correct) {
			this.turn = this.getTurn();
		} else {
			this.turn = {
				prev: this.turn.next,
				next: this.turn.next
			}
		}


		this.quest = next;
		// console.log(this.turn);
		return arr.map(({ id, nickname }) => ({
			id,
			nickname,
			select,
			correct,
			quest: this.quest,
			turn: this.turn
		}))
	}



	/**
	 * 게임 시작
	 * @returns array
	 * @memberof Turn
	 */
	start() {
		// 플레이어 리스트를 섞에 일정한 순서의 패턴을 만듬
		this.pattern = arrayShuffle([...this.players]);
		// 플레이서 순서 패턴을 복사함. 
		this.orders = [...this.pattern];
		// 퀘스트 선택
		this.quest = this.getQuest();
		// 턴 선택
		this.turn = this.getTurn();
		// console.log('this.turn : ', this.players.map(({ id, nickname }) => ({
		// 	id,
		// 	nickname,
		// 	quest: this.quest,
		// 	turn: this.turn
		// })));
		return this.players.map(({ id, nickname }) => ({
			id,
			nickname,
			quest: this.quest,
			turn: this.turn
		}));
	}

	/**
	 * 게임 퇴장
	 * @param {object} { user }
	 * @returns array
	 * @memberof Turn
	 */
	leave({ user }) {
		super.leave({ user });
		if (this.pattern) {
			// 플레이어 순서 패턴에서 나가는 플레이어를 삭제해줌
			this.pattern = this.pattern.filter(el => el.id !== user.id);
			this.orders = [...this.pattern];
			//다음 턴으로 넘김
			if (this.orders.length > 0) {
				this.turn = this.getTurn();
			}
		}
		return this.players.map(({ id, nickname }) => ({
			id,
			nickname,
			quest: this.quest,
			turn: this.turn,
			left: user.nickname
		}));
	}


}

export default Turn;