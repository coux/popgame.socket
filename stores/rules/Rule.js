import { observable } from 'mobx';

class Rule {


	/**
	 * @memberof Rule
	 */

	@observable records = [];
	@observable players = [];


	/**
	 * 게임 옵션 설정
	 * @param {object} { rules }
	 * @memberof Rule
	 */
	setRule({ rules }) {
		this.rules = rules;
	}


	/**
	 * 플레이어 조인
	 * @param {object} { user }
	 * @memberof Rule
	 */
	join({ user }) {
		console.log('rule join', user.id);
		this.players = [...this.players, user];
	}


	/**
	 * 플레이어 퇴장
	 * @param {*} { user }
	 * @memberof Rule
	 */
	leave({ user }) {
		console.log('rule leave', user.id);
		this.players = this.players.filter(player => user.id !== player.id);
	}


	/**
	 * 게임 시작
	 * @memberof Rule
	 */
	start() {
		console.log('game start');
	}


	/**
	 *Creates an instance of Rule.
	 * @memberof Rule
	 */
	constructor() {
		this.score = [];
		this.players = [];
		this.reactions = [];
	}

}

export default Rule;